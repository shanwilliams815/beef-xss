#!/bin/sh

set -e

# Run the beef-xss helper-script
./debian/tests/beef.exp

# test if the password has been changed
if grep -q "passwd: beef" /etc/beef-xss/config.yaml; then
    echo "FAILURE: the password has not been changed"
    exit 1
fi

# test if the service started
if ! systemctl is-active -q beef-xss; then
    echo "FAILURE: beef-xss service is not active"
    exit 1
fi

# test Web UI response
if ! curl -s http://127.0.0.1:3000/ui/authentication | grep "BeEF Authentication"; then
    echo "FAILURE: Web UI test fails"
    exit 1
fi

# stop the service
systemctl stop beef-xss

if systemctl is-active -q beef-xss; then
    echo "FAILURE: beef-xss service is still active"
    exit 1
fi
